package com.daioware.math;

import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;

public class Number {
	
	private static SecureRandom randomGenerator;
	
	static {
		try {
			randomGenerator= SecureRandom.getInstanceStrong();
		} catch (NoSuchAlgorithmException e) {
			randomGenerator=new SecureRandom();
		}
	}

	public static double getRandomNumber(double lowerLimit,double upperLimit){
		return lowerLimit + ( createRandom()* (upperLimit - lowerLimit));
	} 
	private static double createRandom(){
		return randomGenerator.nextDouble();
		//return Math.random();
		//return new java.util.Random().nextDouble();
	}
	/*private static int createRandom(int bound){
		return randomGenerator.nextInt(bound);
	}*/
	
	public static String formatWithoutUnnecessaryZeros(double number) {
		String numberStr=String.valueOf(number);
		int index;
		char c;
		for(index=numberStr.length()-1;index>=0;index--) {
			c=numberStr.charAt(index);
			if(c=='.') {
				index--;
				break;
			}
			else if((c-48)!=0) {
				break;
			}	
			//else keep looking
		}
		return numberStr.substring(0,index+1);
	}

}
