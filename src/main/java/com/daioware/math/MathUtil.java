package com.daioware.math;

public class MathUtil {

	public static double crossMultiplication(double x1,double x2,double y1) {
		return (x1<y1)?x1*x2/y1:y1*x2/x1;
	}
	public static double crossMultiplication(double x1,double y1) {
		return crossMultiplication(x1,100,y1);
	}
}
