package com.daioware.math;

import org.junit.Test;

import com.daioware.math.Number;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.List;

public class RandomNumberTest {

	@Test
	public void test() {
		double lower=0,upper=10000;
		List<Double> numbers=new ArrayList<>(10);
		for(int i=0;i<10;i++) {
			numbers.add(Number.getRandomNumber(lower, upper+1));
		}
		numbers.forEach(System.out::println);
		assertTrue(!numbers.contains(lower-1)&&!numbers.contains(upper+1));
	}
}
